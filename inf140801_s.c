#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
typedef struct msg
{
    long type;
    char text[1024];
} message;
int num_of_aditional_arguments(char str[1024])//Returns number of spaces of a message
{                                             //which is equal to (numer of words - 1)
    int c = 0;
    
    for (int i = 0; str[i] != '\0'; i++)
    {
        if (str[i] == ' ')
        {
            c++;
        }
    }
    return c;
}

int main(int argc, char* argv[])
{
    
    FILE *file;
    int n, g;
    char username[32], password[32];
    
    file = fopen("conf_file.txt", "r");
    
    if (file == NULL) 
    {
        fprintf(stderr, "Can't open input file conf_file!\n");
        fclose(file);
        return 0;
    }
    
    fscanf(file,"%d",&n);//number of users
    fscanf(file,"%d",&g);//number of groups
	
	
    char logged[n][32];
    char usernames[n][32], passwords[n][32];
    for (int i = 0; i < n; i++)//getting user info from conf file
    {
        fscanf(file, "%s %s", username, password);
        strcpy(usernames[i], username);
        strcpy(passwords[i], password);
    }
    fclose(file);
    
    int mid = msgget(0x123, 0600 | IPC_CREAT); //Message queue for all future 
    message first;                             //connections and a corresponding
                                               //structure
    first.type = 6; //Common type of message in the common queue
                    //used only to establish new queues for each user
    int queues[n];//array of client-server queues
    message msgfc[n], msgtc[n],msgtcr[n]; //Message structures from and to clients
    char str_i[16];//Buffer for sending queue numbers via common queue
	
    for (int i = 0; i < n ; i++)//establishing communication with clients
    {   
        sprintf(str_i,"%d",1000 + i);//int-->string
        strcpy(first.text, str_i);
        msgsnd(mid, &first, strlen(first.text) + 1, 0); //Sending future queue number
        
        msgfc[i].type = 1; //Message from client
        msgtc[i].type = 2; //Message to client
        msgtcr[i].type = 3; //Message to client recieving message from another client
        queues[i] = msgget(1000 + i, 0600 | IPC_CREAT); //New queue for i client
        strcpy(msgtc[i].text, "Welcome to my communicator, log in please");
        msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
    }
    char groups[g][n][32];//names of groups are just nubers
    int ns = -1;//number of spaces in user generated message
    char* wrd1;//words used as user given command-building blocks
    char* wrd2;
    char* wrd3;
    char msgfc_c[1024];
    for (int j = 0; j < n; j++)//initiating groups and logged arrays
    {
        strcpy(logged[j], " ");
        for(int i = 0;i < g; i++)
        {
            strcpy(groups[i][j], " ");
        }
    }
    
    
    while(1)//main communication loop
    {   
        for (int i = 0; i < n; i++)
        {
            int msg_check = msgrcv(queues[i], &msgfc[i], 1024, 1, IPC_NOWAIT);
            if (msg_check != -1)
            {   
                ns = num_of_aditional_arguments(msgfc[i].text);//Checking number of words in a message
                if (ns == 0)
                {
                    if(strcmp(logged[i]," ") != 0)
                    {
                        //listu
                        if (strcmp(msgfc[i].text,"listu") == 0)
                        {
                            for (int u = 0; u < n; u++)
                            {
                                strcpy(msgtc[i].text,logged[u]);
                                msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                            }
                        }
                        //listg
                        else if (strcmp(msgfc[i].text,"listg") == 0)
                        {
                            for (int u = 0; u < g; u++)
                            {
                                sprintf(msgtc[i].text,"%d",u);
                                msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                            }
                        }
                        
                        //logout
                        else if (strcmp(msgfc[i].text,"logout") == 0)
                        {
                            strcpy(logged[i], " ");
                            strcpy(msgtc[i].text, "Logout succesful!");
                            msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                        }
                        else
                        {
                            strcpy(msgtc[i].text, "Unknown command");
                            msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                        }
                    }
                    else
                    {
                        strcpy(msgtc[i].text, "Log in first!");
                        msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                    }
                }
                if (ns == 1)
                {
                    if(strcmp(logged[i]," ") != 0)
                    {
                        strcpy(msgfc_c, msgfc[i].text);//copy msg from client
                        wrd1 = strtok(msgfc_c," ");//command
                        wrd2 = strtok(NULL,"\0");//firs argument
                        int er = 0;//wrong group finder variable
                        char gn[2];//group number as a string
                        //listuing
                        if (strcmp(wrd1,"listuing") == 0)
                        {
                            for (int u = 0; u < g; u++)
                            {
                                sprintf(gn,"%d",u);
                                if (strcmp(wrd2,gn) == 0)
                                {
                                    if(strcmp(groups[u][i]," ") != 0)
                                    {
                                        for (int gu = 0; gu < n; gu++)
                                        {
                                            strcpy(msgtc[i].text, groups[u][gu]);
                                            msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                                        }
                                        
                                    }
                                    else
                                    {
                                        strcpy(msgtc[i].text, "Sorry, you are not in that group");
                                        msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                                    }
                                    er = 1;
                                }
                            }
                            if (er == 0)
                            {
                                strcpy(msgtc[i].text, "There is no such group");
                                msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                            }
                        }
                        //joing
                        else if (strcmp(wrd1,"joing") == 0)
                        {
                            
                            for (int u = 0; u < g; u++)
                            {
                                sprintf(gn,"%d",u);
                                if (strcmp(wrd2,gn) == 0)
                                {
                                    strcpy(groups[u][i], logged[i]);
                                    strcpy(msgtc[i].text, "Group joined succesfully!");
                                    msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                                    er = 1;
                                }
                            }
                            if (er == 0)
                            {
                                strcpy(msgtc[i].text, "There is no such group");
                                msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                            }
                        }
                        //leaveg
                        else if (strcmp(wrd1,"leaveg") == 0)
                        {
                            
                            for (int u = 0; u < g; u++)
                            {
                                sprintf(gn,"%d",u);
                                if (strcmp(wrd2,gn) == 0)
                                {
                                    strcpy(groups[u][i], " ");
                                    strcpy(msgtc[i].text, "Group left succesfully!");
                                    msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                                    er = 1;
                                }
                            }
                            if (er == 0)
                            {
                                strcpy(msgtc[i].text, "There is no such group");
                                msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                            }
                            
                        }
                        else
                        {
                            strcpy(msgtc[i].text, "Unknown command");
                            msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                        }
                    }
                    else
                    {
                        strcpy(msgtc[i].text, "Log in first!");
                        msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                    }
                }
                if (ns >= 2)
                {
                    //login
                    strcpy(msgfc_c, msgfc[i].text);//copy msg from client
                    wrd1 = strtok(msgfc_c," ");//command
                    wrd2 = strtok(NULL," ");//firs argument
                    wrd3 = strtok(NULL,"\0");//second argument
                    int logsuc = 0;//login succes check variable
                    char gn[2];//group number as a string
                    int x = n;//alredy logged in check variable
                    int rnf = 0;//msg reciever not found variable
                    if (strcmp(wrd1,"login") == 0)
                    {
                        for (int u = 0; u < n; u++)
                        {
                            if (strcmp(wrd2,usernames[u]) == 0)
                            {
                                if (strcmp(wrd3,passwords[u]) == 0)
                                {
                                    for (int j = 0; j < n; j++)
                                    {   
                                        if (strcmp(wrd2,logged[j]) == 0)
                                        {
                                        strcpy(msgtc[i].text, "Alredy logged in");
                                        msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                                        x++;
                                        logsuc++;
                                        }
                                        x--;
                                    }
                                    if(x == 0)
                                    {
                                        strcpy(msgtc[i].text, "Log in succesful!");
                                        msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                                        strcpy(logged[i], usernames[u]);
                                        logsuc++;
                                    }
                                }
                            }
                        }
                        if(logsuc == 0)
                            {
                                strcpy(msgtc[i].text, "Wrong login or password");
                                msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                            }
                    }
                    else if(strcmp(logged[i]," ") != 0)
                    {
                        //msgtu
                        if (strcmp(wrd1,"msg") == 0)
                        {
                            for (int u = 0; u < n; u++)
                            {
                                if (strcmp(wrd2,logged[u]) == 0)
                                {   
                                    strcpy(msgtcr[u].text,"Msg from ");
                                    strcat(msgtcr[u].text, logged[i]);
                                    strcat(msgtcr[u].text, ":");
                                    strcat(msgtcr[u].text, wrd3);
                                    msgsnd(queues[u], &msgtcr[u], strlen(msgtcr[u].text) + 1, 0);
                                    strcpy(msgtc[i].text, "Message sent!");
                                    msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                                    rnf++;
                                }
                            }
                            if (rnf == 0)
                            {
                                strcpy(msgtc[i].text, "Sorry, reciever(s) not logged");
                                msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                            }
                        }
                        //msgtg
                        else if (strcmp(wrd1,"msgtg") == 0)
                        {
                            for (int u = 0; u < g; u++)
                            {
                                sprintf(gn,"%d",u);
                                if (strcmp(wrd2,gn) == 0)
                                {   
                                    if(strcmp(groups[u][i]," ") != 0)
                                    {
                                        for(int um = 0; um < n; um++)
                                        {
                                            if(strcmp(groups[u][um]," ") != 0)
                                            {
                                                strcpy(msgtcr[um].text, "Msg from ");
                                                strcat(msgtcr[um].text, logged[i]);
                                                strcat(msgtcr[um].text, " to group ");
                                                strcat(msgtcr[um].text, wrd2);
                                                strcat(msgtcr[um].text, " :");
                                                strcat(msgtcr[um].text, wrd3);
                                                msgsnd(queues[um], &msgtcr[um], strlen(msgtcr[um].text) + 1, 0);
                                            }
                                        }
                                        strcpy(msgtc[i].text, "Message sent!");
                                        msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                                    }
                                    else
                                    {
                                        strcpy(msgtc[i].text, "Sorry, you are not in that group");
                                        msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                                    }
                                }
                            }
                        }
                        else
                        {
                            strcpy(msgtc[i].text, "Unknown command");
                            msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                        }
                    }
                    if(strcmp(logged[i]," ") == 0)
                        if (strcmp(wrd1,"login") != 0)
                        {
                            strcpy(msgtc[i].text, "Log in first!");
                            msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                        }
                }

            }      
            if (strcmp(msgfc[i].text,"finish") == 0)//finishing with server
            {
                msgctl(mid, IPC_RMID, NULL);
                for (int i = 0; i < n; i++)
                    {
                        msgctl(queues[i], IPC_RMID, NULL);
                    } 
                
                return 0;
            }
            if (strcmp(msgfc_c,"") != 0)
            {
                if (strcmp(msgtc[i].text,"") == 0)
                {
                    strcpy(msgtc[i].text, "Wrong command");
                    msgsnd(queues[i], &msgtc[i], strlen(msgtc[i].text) + 1, 0);
                }
                
            }
            
        }
    }


	msgctl(mid, IPC_RMID, NULL);
    for (int i = 0; i < n; i++)
        {
            msgctl(queues[i], IPC_RMID, NULL);
        } 
    
	return 0;
}
