#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
typedef struct msg
{
    long type;
    char text[1024];
} message;
int main()
{
	int mid = msgget(0x123, 0600 | IPC_CREAT);
	
    message first, msgfc, msgtc, msgtcr;
    msgfc.type = 1; //message from client
    msgtc.type = 2; //message to client
    msgtcr.type = 3;//message to client recieve (messages)
    first.type = 6; //massage type for all the clients 
                    //is the same, it comes from common queue
    
    
    msgrcv(mid, &first, 1024, 6, 0);
    int que_num = atoi(first.text);//string --> int
    int queue = msgget(que_num, 0600 | IPC_CREAT);//unique queue number used to make a 
                                                  //separate queue for this user only
    
    int msg_check = msgrcv(queue, &msgtc, 1024, 2, IPC_NOWAIT);
        if (msg_check != -1)
        {
            printf("%s\n",msgtc.text);
        }
    
    while(1)//user - server interaction loop
    {   
        fgets(msgfc.text,1024,stdin);//user input
        msgfc.text[strlen(msgfc.text) - 1] = '\0';
        if (strcmp(msgfc.text,"getmsg") != 0)
        {
            msgsnd(queue, &msgfc, strlen(msgfc.text) + 1, 0);
            sleep(1);//in case the sever needs a bit more time to process everyting
            for (int j = 0; j < 100; j++)//Loop is instantaneous and it's number of repeats 
            {                            //prevents the message from being cut into pieces
                int msg_check = msgrcv(queue, &msgtc, 1024, 2, IPC_NOWAIT);
                if (msg_check != -1)
                {
                    printf("%s\n",msgtc.text);
                    
                }
            }
        }
        else
        {
            int msgr = 0;//msg recieved test
            for (int j = 0; j < 100; j++)//Loop is instantaneous and it's number of repeats 
            {                            //prevents the message from being cut into pieces
                int msg_check = msgrcv(queue, &msgtcr, 1024, 3, IPC_NOWAIT);
                if (msg_check != -1)
                {
                    printf("%s\n",msgtcr.text);
                    msgr++;
                }
            }
            if (msgr == 0)
            {
                printf("No messages in your inbox\n");
            }
        }
        
    }
	msgctl(mid, IPC_RMID, NULL);
    msgctl(queue, IPC_RMID, NULL);
	return 0;
}
