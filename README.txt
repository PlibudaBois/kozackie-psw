Kompilacja:
-należy użyć komendy "make" w linii komend
Uruchomienie:
-do terminala należy wpisać "./server"
-dla każdego kolejnego klienta należy otworzyć nowe okno terminala i wpisać "./klient"

Plik inf140801_s.c zawiera kod serwera.
Serwer zajmuje się operacjami na tekście, który wysyłają klienci i odsyłaniu do klientów informacji zwrotnych.

Plik inf140801_c.c zawiera kod klienta.
Klient czyta ze standarodowego wejścia, to co wpisze użytkownik, wysyła tekst do serwera i wypisuje na ekran informację zwrotną.

Komendy i szczegółowy opis działania systemu są dostępne w pliku PROTOCOL.txt.
